package cz.utb.fai.fbimostwantedlist.activity

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.android.volley.toolbox.Volley
import com.google.gson.reflect.TypeToken
import cz.utb.fai.fbimostwantedlist.R
import cz.utb.fai.fbimostwantedlist.adapter.CustomPagerAdapter
import cz.utb.fai.fbimostwantedlist.model.person.Person
import cz.utb.fai.fbimostwantedlist.model.request.GsonGetRequest
import cz.utb.fai.fbimostwantedlist.util.FileConstants
import cz.utb.fai.fbimostwantedlist.util.Utils

class DetailActivity : AppCompatActivity() {
    val TAG = this::class.java.name

    lateinit var person: Person

    override fun onCreate(savedInstanceState: Bundle?) {
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val bundle = intent.extras

        if (bundle != null) {
            person = bundle.getParcelable("person")
                    ?: throw IllegalStateException("Did not recieve any person to display")
        }

        val viewPager = findViewById<ViewPager>(R.id.viewpager)
        viewPager.adapter = CustomPagerAdapter(this, person.images)

        supportActionBar?.title = "Detail"

        populatePage()
    }

    private fun populatePage() {
        setText(R.id.detailTitle, person.title)
        setText(R.id.detailAliases, person.aliases?.joinToString("\n")
                ?: getString(R.string.not_specified))

        setText(R.id.detailDetails, Html.fromHtml(person.details
                ?: getString(R.string.not_specified), Html.FROM_HTML_MODE_LEGACY).toString())
        setText(R.id.detailAdditionalInformation, Html.fromHtml(person.additionalInformation
                ?: getString(R.string.not_specified), Html.FROM_HTML_MODE_LEGACY).toString())
        setText(R.id.detailCaution, Html.fromHtml(person.caution
                ?: getString(R.string.not_specified), Html.FROM_HTML_MODE_LEGACY).toString())
        setText(R.id.detailWarningMessage, Html.fromHtml(person.warningMessage
                ?: getString(R.string.not_specified), Html.FROM_HTML_MODE_LEGACY).toString())

        setText(R.id.detailStatus, person.status?.toString() ?: getString(R.string.not_specified))
        setText(R.id.detailClassification, person.personClassification?.toString()
                ?: getString(R.string.not_specified))
        setText(R.id.detailSex, person.sex ?: getString(R.string.not_specified))
        setText(R.id.detailRace, person.raceRaw ?: getString(R.string.not_specified))
        setText(R.id.detailNationality, person.nationality ?: getString(R.string.not_specified))
        setText(R.id.detailPlaceOfBirth, person.placeOfBirth ?: getString(R.string.not_specified))
        setText(R.id.detailDescription, person.description ?: getString(R.string.not_specified))

        setText(R.id.detailSuspects, person.suspects?.joinToString("\n")
                ?: getString(R.string.not_specified))
        setText(R.id.detailSubjects, person.subjects?.joinToString("\n")
                ?: getString(R.string.not_specified))
        setText(R.id.detailFieldOffices, person.fieldOffices?.joinToString("\n")
                ?: getString(R.string.not_specified))
        setText(R.id.detailLanguages, person.languages?.joinToString("\n")
                ?: getString(R.string.not_specified))

        setText(R.id.detailAgeMin, person.ageMin?.toString() ?: getString(R.string.not_specified))
        setText(R.id.detailAgeMax, person.ageMax?.toString() ?: getString(R.string.not_specified))
        setText(R.id.detailHeightMin, person.heightMin?.toString()
                ?: getString(R.string.not_specified))
        setText(R.id.detailHeightMax, person.heightMax?.toString()
                ?: getString(R.string.not_specified))
        setText(R.id.detailWeightMin, person.weightMin?.toString()
                ?: getString(R.string.not_specified))
        setText(R.id.detailWeightMax, person.weightMax?.toString()
                ?: getString(R.string.not_specified))

        setText(R.id.detailEyes, person.eyes ?: getString(R.string.not_specified))
        setText(R.id.detailHair, person.hair ?: getString(R.string.not_specified))

        setText(R.id.detailScarsAndMarks, person.scarsAndMarks ?: getString(R.string.not_specified))
        setText(R.id.detailBuild, person.build ?: getString(R.string.not_specified))

        setText(R.id.detailRewardMin, person.rewardMin?.toString()
                ?: getString(R.string.not_specified))
        setText(R.id.detailRewardMax, person.rewardMax?.toString()
                ?: getString(R.string.not_specified))
        setText(R.id.detailRewardInformation, person.rewardText
                ?: getString(R.string.not_specified))

        setText(R.id.detailOccupations, person.occupations?.joinToString("\n")
                ?: getString(R.string.not_specified))
        setText(R.id.detailPossibleCountries, person.possibleCountries?.joinToString("\n")
                ?: getString(R.string.not_specified))
        setText(R.id.detailPossibleStates, person.possibleStates?.joinToString("\n")
                ?: getString(R.string.not_specified))
        setText(R.id.detailCoordinates, if (person.coordinates.isNullOrEmpty()) getString(R.string.not_specified) else person.coordinates!!.joinToString("\n"))
        setText(R.id.detailLocations, person.locations?.joinToString("\n")
                ?: getString(R.string.not_specified))
    }

    private fun setText(resId: Int, text: String) {
        val textView = findViewById<TextView>(resId)
        textView.text = text
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.favorite, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }

        if (item.itemId == R.id.favorite) {
            Toast.makeText(this, "Adding to Stored Persons ...", Toast.LENGTH_SHORT).show()
            savePersonToLocalStorage()
            Toast.makeText(this, "Added", Toast.LENGTH_SHORT).show()
            return true
        }

        if (item.itemId == R.id.refresh) {
            Toast.makeText(this, "Refreshing ...", Toast.LENGTH_SHORT).show()
            refresh()
            return true
        }

        return false
    }

    private fun savePersonToLocalStorage() {
        val gson = Utils.genericGson
        val sharedPreferences: SharedPreferences = this.getSharedPreferences(FileConstants.STORED_PERSONS_PREFERENCE_FILE, MODE_PRIVATE)

        val setOfSavedPersonsJson = sharedPreferences.getString(FileConstants.STORED_PERSONS_KEY, "[]")!!
        val setOfSavedPersons: MutableMap<String, Person> = gson.fromJson(setOfSavedPersonsJson, object : TypeToken<MutableMap<String, Person>>() {}.type)

        setOfSavedPersons.put(person.uid, person)

        with(sharedPreferences.edit()) {
            putString(FileConstants.STORED_PERSONS_KEY, gson.toJson(setOfSavedPersons))
            apply()
        }
    }

    private fun savePersonToLocalStorageIfThere() {
        val gson = Utils.genericGson
        val sharedPreferences: SharedPreferences = this.getSharedPreferences(FileConstants.STORED_PERSONS_PREFERENCE_FILE, MODE_PRIVATE)

        val setOfSavedPersonsJson = sharedPreferences.getString(FileConstants.STORED_PERSONS_KEY, "[]")!!
        val setOfSavedPersons: MutableMap<String, Person> = gson.fromJson(setOfSavedPersonsJson, object : TypeToken<MutableMap<String, Person>>() {}.type)

        if (setOfSavedPersons.containsKey(person.uid)) {
            setOfSavedPersons.put(person.uid, person)

            with(sharedPreferences.edit()) {
                putString(FileConstants.STORED_PERSONS_KEY, gson.toJson(setOfSavedPersons))
                apply()
            }
        }
    }

    override fun onBackPressed() {
        val returnIntent = Intent()
        returnIntent.putExtra("backPressed", true)
        setResult(Activity.RESULT_OK, returnIntent)
        super.onBackPressed()
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }

    private fun refresh() {
        val queue = Volley.newRequestQueue(this)
        val url = person.id
        val gsonRequest = GsonGetRequest(
                url,
                Person::class.java,
                null,
                {
                    this.person = it
                    populatePage()
                    savePersonToLocalStorageIfThere()
                    Toast.makeText(this, "Refreshed", Toast.LENGTH_SHORT).show()
                },
                {
                    Toast.makeText(
                            applicationContext,
                            "Ooopsie ... something did not work ...",
                            Toast.LENGTH_LONG
                    ).show()
                    Log.d(TAG, it.toString())
                })

        queue.add(gsonRequest)
    }
}
package cz.utb.fai.fbimostwantedlist.model.person

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import cz.utb.fai.fbimostwantedlist.util.Utils
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZoneOffset

data class Person(
        @SerializedName("@id")
        val id: String,
        val uid: String,
        val title: String,
        val legatNames: List<String>?,
        val sex: String?,
        val race: String?,
        val raceRaw: String?,
        val nationality: String?,
        val status: PersonStatus?,
        val placeOfBirth: String?,
        val personClassification: PersonClassification?,
        val aliases: List<String>?,

        val description: String?,
        val details: String?,
        val additionalInformation: String?,
        val datesOfBirthUsed: List<String>?,
        val caution: String?,
        val warningMessage: String?,
        val suspects: List<String>?,
        val subjects: List<String>?,
        val fieldOffices: List<String>?,
        val languages: List<String>?,

        val ageMin: Int?,
        val ageMax: Int?,
        val ageRange: String?,

        val heightMin: Int?,
        val heightMax: Int?,

        val weight: String?,
        val weightMin: Int?,
        val weightMax: Int?,

        val eyes: String?,
        val eyesRaw: String?,
        val hair: String?,
        val hairRaw: String?,
        val scarsAndMarks: String?,
        val complexion: String?,
        val build: String?,

        val remarks: String?,
        val occupations: List<String>?,
        val possibleCountries: List<String>?,
        val possibleStates: List<String>?,
        val coordinates: List<String>?,
        val locations: List<String>?,

        val rewardMin: Int?,
        val rewardMax: Int?,
        val rewardText: String?,

        val url: String,
        val path: String,
        val files: List<FBIFile>,
        val images: List<FBIImage>,
        val publicationDate: LocalDateTime?,
        val modified: LocalDateTime?,

        val ncic: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString()!!,
            parcel.readString()!!,
            parcel.readString()!!,
            parcel.createStringArrayList(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            Utils.transformIfNotNull(parcel.readString()) { PersonStatus.valueOf(it) },
            parcel.readString(),
            Utils.transformIfNotNull(parcel.readString()) { PersonClassification.valueOf(it) },
            parcel.createStringArrayList(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.createStringArrayList(),
            parcel.readString(),
            parcel.readString(),
            parcel.createStringArrayList(),
            parcel.createStringArrayList(),
            parcel.createStringArrayList(),
            parcel.createStringArrayList(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.createStringArrayList(),
            parcel.createStringArrayList(),
            parcel.createStringArrayList(),
            parcel.createStringArrayList(),
            parcel.createStringArrayList(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString()!!,
            parcel.readString()!!,
            parcel.createTypedArrayList(FBIFile)!!.toList(),
            parcel.createTypedArrayList(FBIImage)!!.toList(),
            Utils.transformIfNotNull(parcel.readString()) { LocalDateTime.ofInstant(Instant.ofEpochMilli(it.toLong()), ZoneId.systemDefault()) },
            Utils.transformIfNotNull(parcel.readString()) { LocalDateTime.ofInstant(Instant.ofEpochMilli(it.toLong()), ZoneId.systemDefault()) },
            parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(uid)
        parcel.writeString(title)
        parcel.writeStringList(legatNames)
        parcel.writeString(sex)
        parcel.writeString(race)
        parcel.writeString(raceRaw)
        parcel.writeString(nationality)
        parcel.writeString(status?.name)
        parcel.writeString(placeOfBirth)
        parcel.writeString(personClassification?.name)
        parcel.writeStringList(aliases)
        parcel.writeString(description)
        parcel.writeString(details)
        parcel.writeString(additionalInformation)
        parcel.writeStringList(datesOfBirthUsed)
        parcel.writeString(caution)
        parcel.writeString(warningMessage)
        parcel.writeStringList(suspects)
        parcel.writeStringList(subjects)
        parcel.writeStringList(fieldOffices)
        parcel.writeStringList(languages)
        parcel.writeValue(ageMin)
        parcel.writeValue(ageMax)
        parcel.writeString(ageRange)
        parcel.writeValue(heightMin)
        parcel.writeValue(heightMax)
        parcel.writeString(weight)
        parcel.writeValue(weightMin)
        parcel.writeValue(weightMax)
        parcel.writeString(eyes)
        parcel.writeString(eyesRaw)
        parcel.writeString(hair)
        parcel.writeString(hairRaw)
        parcel.writeString(scarsAndMarks)
        parcel.writeString(complexion)
        parcel.writeString(build)
        parcel.writeString(remarks)
        parcel.writeStringList(occupations)
        parcel.writeStringList(possibleCountries)
        parcel.writeStringList(possibleStates)
        parcel.writeStringList(coordinates)
        parcel.writeStringList(locations)
        parcel.writeValue(rewardMin)
        parcel.writeValue(rewardMax)
        parcel.writeString(rewardText)
        parcel.writeString(url)
        parcel.writeString(path)
        parcel.writeTypedList(files)
        parcel.writeTypedList(images)
        parcel.writeString(publicationDate?.toInstant(ZoneOffset.UTC)?.toEpochMilli()?.toString())
        parcel.writeString(modified?.toInstant(ZoneOffset.UTC)?.toEpochMilli()?.toString())
        parcel.writeString(ncic)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Person> {
        override fun createFromParcel(parcel: Parcel): Person {
            return Person(parcel)
        }

        override fun newArray(size: Int): Array<Person?> {
            return arrayOfNulls(size)
        }
    }
}


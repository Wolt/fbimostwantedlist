package cz.utb.fai.fbimostwantedlist.util

object FileConstants {
    val STORED_PERSONS_KEY = "saved_persons"
    val STORED_PERSONS_PREFERENCE_FILE = "saved_persons"
    val SEARCH_RESULTS_KEY = "search_history"
    val SEARCH_RESULTS_PREFERENCE_FILE = "search_history"
}
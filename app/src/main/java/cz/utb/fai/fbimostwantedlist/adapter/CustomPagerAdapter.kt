package cz.utb.fai.fbimostwantedlist.adapter

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import com.squareup.picasso.Picasso
import cz.utb.fai.fbimostwantedlist.R
import cz.utb.fai.fbimostwantedlist.model.person.FBIImage


class CustomPagerAdapter(private val mContext: Context, var images: List<FBIImage>) : PagerAdapter() {
    override fun instantiateItem(collection: ViewGroup, position: Int): Any {
        val inflater = LayoutInflater.from(mContext)
        val layout = inflater.inflate(R.layout.page, collection, false) as ViewGroup

        val imageView = layout.getChildAt(0) as ImageView

        if (!TextUtils.isEmpty(images[position].original)) {
            Picasso.with(mContext).load(images[position].original)
                    .error(R.drawable.placeholder)
                    .placeholder(R.drawable.placeholder)
                    .into(imageView)
        }

        collection.addView(layout)
        return layout
    }

    override fun destroyItem(collection: ViewGroup, position: Int, view: Any) {
        collection.removeView(view as View)
    }

    override fun getCount(): Int {
        return images.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getPageTitle(position: Int): CharSequence {
        return "Images"
    }
}
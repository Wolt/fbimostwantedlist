package cz.utb.fai.fbimostwantedlist.model.search

enum class SortOn(val value: String) {
    PUBLICATION("publication"),
    MODIFIED("modified")
}
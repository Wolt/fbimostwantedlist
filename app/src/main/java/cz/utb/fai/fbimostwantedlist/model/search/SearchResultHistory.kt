package cz.utb.fai.fbimostwantedlist.model.search

import cz.utb.fai.fbimostwantedlist.model.person.PersonClassification
import cz.utb.fai.fbimostwantedlist.model.person.PersonStatus

data class SearchResultHistory(
        val pageSize: Int,
        val sortOn: SortOn,
        val sortOrder: SortOrder,
        val title: String?,
        val fieldOffices: String?,
        val personClassification: PersonClassification?,
        val status: PersonStatus?,
        val total: Int
)

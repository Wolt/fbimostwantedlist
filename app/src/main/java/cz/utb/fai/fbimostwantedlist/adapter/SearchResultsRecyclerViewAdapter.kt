package cz.utb.fai.fbimostwantedlist.adapter

import android.content.Context
import android.content.SharedPreferences
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.reflect.TypeToken
import cz.utb.fai.fbimostwantedlist.R
import cz.utb.fai.fbimostwantedlist.model.search.SearchResultHistory
import cz.utb.fai.fbimostwantedlist.util.FileConstants
import cz.utb.fai.fbimostwantedlist.util.Utils

class SearchResultsRecyclerViewAdapter(val context: Context, var searchResults: Set<SearchResultHistory>, private val onItemClickListener: (SearchResultHistory) -> Unit) : RecyclerView.Adapter<SearchResultsRecyclerViewAdapter.SearchResultsViewHolder>() {
    class SearchResultsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val titleTextView: TextView
        val fieldOfficeTextView: TextView
        val statusTextView: TextView
        val classificationTextView: TextView
        val sortOnTextView: TextView
        val sortOrderTextView: TextView
        val pageSizeTextView: TextView
        val totalTextView: TextView
        val cardView: CardView

        init {
            titleTextView = view.findViewById(R.id.searchResultsTitle) as TextView
            fieldOfficeTextView = view.findViewById(R.id.searchResultsFieldOffice) as TextView
            statusTextView = view.findViewById(R.id.searchResultsStatus) as TextView
            classificationTextView = view.findViewById(R.id.searchResultsClassification) as TextView
            sortOnTextView = view.findViewById(R.id.searchResultsSortOn) as TextView
            sortOrderTextView = view.findViewById(R.id.searchResultsSortOrder) as TextView
            pageSizeTextView = view.findViewById(R.id.searchResultsPageSize) as TextView
            totalTextView = view.findViewById(R.id.searchResultsTotal) as TextView
            cardView = view.findViewById(R.id.cardview_search_results) as CardView
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): SearchResultsViewHolder {
        val view: View = LayoutInflater.from(viewGroup.context).inflate(R.layout.list_row_search_results, null)
        return SearchResultsViewHolder(view)
    }

    override fun onBindViewHolder(searchResultsViewHolder: SearchResultsViewHolder, i: Int) {
        val searchResult = searchResults.elementAt(i)

        searchResultsViewHolder.titleTextView.text = if (searchResult.title == null) "Not Specified" else Html.fromHtml(searchResult.title, Html.FROM_HTML_MODE_LEGACY)
        searchResultsViewHolder.fieldOfficeTextView.text = if (searchResult.fieldOffices == null) "Not Specified" else Html.fromHtml(searchResult.fieldOffices, Html.FROM_HTML_MODE_LEGACY)
        searchResultsViewHolder.statusTextView.text = if (searchResult.status == null) "Not Specified" else Html.fromHtml(searchResult.status.toString(), Html.FROM_HTML_MODE_LEGACY)
        searchResultsViewHolder.classificationTextView.text = if (searchResult.personClassification == null) "Mot Specified" else Html.fromHtml(searchResult.personClassification.toString(), Html.FROM_HTML_MODE_LEGACY)
        searchResultsViewHolder.sortOnTextView.text = Html.fromHtml(searchResult.sortOn.toString(), Html.FROM_HTML_MODE_LEGACY)
        searchResultsViewHolder.sortOrderTextView.text = Html.fromHtml(searchResult.sortOrder.toString(), Html.FROM_HTML_MODE_LEGACY)
        searchResultsViewHolder.pageSizeTextView.text = Html.fromHtml(searchResult.pageSize.toString(), Html.FROM_HTML_MODE_LEGACY)
        searchResultsViewHolder.totalTextView.text = Html.fromHtml(searchResult.total.toString(), Html.FROM_HTML_MODE_LEGACY)

        val listener: View.OnClickListener = View.OnClickListener {
            onItemClickListener.invoke(searchResult)
        }
        searchResultsViewHolder.cardView.setOnClickListener(listener)

        searchResultsViewHolder.cardView.setOnLongClickListener {
            val pop = PopupMenu(context, it)
            pop.inflate(R.menu.context_menu)
            pop.setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.menu_custom_delete -> {
                        val p = searchResults.toMutableSet()
                        p.remove(searchResult)
                        searchResults = p.toSet()
                        removeFromLocalStorage(searchResult)
                        this.notifyDataSetChanged()
                    }
                }
                true
            }
            pop.show()
            true
        }
    }

    private fun removeFromLocalStorage(searchResult: SearchResultHistory) {
        val gson = Utils.genericGson
        val sharedPreferences: SharedPreferences = context.getSharedPreferences(FileConstants.SEARCH_RESULTS_PREFERENCE_FILE, AppCompatActivity.MODE_PRIVATE)

        val setOfSearchHistoryJson = sharedPreferences.getString(FileConstants.SEARCH_RESULTS_KEY, "[]")!!
        val setOfSearchHistory: MutableSet<SearchResultHistory> = gson.fromJson(setOfSearchHistoryJson, object : TypeToken<MutableSet<SearchResultHistory>>() {}.type)

        setOfSearchHistory.remove(searchResult)

        with(sharedPreferences.edit()) {
            putString(FileConstants.SEARCH_RESULTS_KEY, gson.toJson(setOfSearchHistory))
            apply()
        }
    }

    override fun getItemCount() = searchResults.size
}
package cz.utb.fai.fbimostwantedlist.model.search

import cz.utb.fai.fbimostwantedlist.model.person.PersonClassification
import cz.utb.fai.fbimostwantedlist.model.person.PersonStatus

/*
* pagesize - int
* page - int
* sort_on - string (modified or publication)
* sort_order - string (desc or asc)
* title - string (title of the wanted person)
* field_offices - string (field office responsible)
* person_classification - PersonClassification (Man, Victim, Accomplice)
* status - PersonStatus (na, captured, recovered, located, surrendered, deceased)
* */

//val url = "https://api.fbi.gov/@wanted?pageSize=10&page=${page}&sort_on=modified&sort_order=desc"

object SearchUrl {
    var page: Int = 1
    var pageSize: Int = 10
    var sortOn: SortOn = SortOn.MODIFIED
    var sortOrder: SortOrder = SortOrder.DESC

    var title: String? = null
    var fieldOffices: String? = null
    var personClassification: PersonClassification? = null
    var status: PersonStatus? = null

    fun getUrl(): String {
        val urlBuilder = StringBuilder("https://api.fbi.gov/@wanted?pageSize=${pageSize}&page=${page}&sort_on=${sortOn.value}&sort_order=${sortOrder.value}")

        if (title != null) urlBuilder.append("&title=${title}")
        if (fieldOffices != null) urlBuilder.append("&field_offices=${fieldOffices}")
        if (personClassification != null) urlBuilder.append("&person_classification=${personClassification}")
        if (status != null) urlBuilder.append("&status=${status}")

        return urlBuilder.toString()
    }
}
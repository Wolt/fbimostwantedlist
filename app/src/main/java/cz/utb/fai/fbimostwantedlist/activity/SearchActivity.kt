package cz.utb.fai.fbimostwantedlist.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.textfield.TextInputEditText
import cz.utb.fai.fbimostwantedlist.R
import cz.utb.fai.fbimostwantedlist.model.person.PersonClassification
import cz.utb.fai.fbimostwantedlist.model.person.PersonStatus
import cz.utb.fai.fbimostwantedlist.model.search.SearchUrl
import cz.utb.fai.fbimostwantedlist.model.search.SortOn
import cz.utb.fai.fbimostwantedlist.model.search.SortOrder

class SearchActivity : AppCompatActivity() {

    val TAG = this::class.java.name

    lateinit var title: TextInputEditText
    lateinit var fieldOffice: TextInputEditText
    lateinit var classification: AutoCompleteTextView
    lateinit var status: AutoCompleteTextView
    lateinit var pageSize: AutoCompleteTextView
    lateinit var sortOn: AutoCompleteTextView
    lateinit var sortOrder: AutoCompleteTextView


    override fun onCreate(savedInstanceState: Bundle?) {
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        supportActionBar?.title = "Search"

        title = findViewById(R.id.textEditTitle)
        fieldOffice = findViewById(R.id.textEditFieldOffice)
        classification = findViewById(R.id.autoCompleteClassification)
        status = findViewById(R.id.autoCompleteStatus)
        pageSize = findViewById(R.id.autoCompletePageSize)
        sortOn = findViewById(R.id.autoCompleteSortOn)
        sortOrder = findViewById(R.id.autoCompleteSortOrder)

        populateFields()
    }

    private fun populateFields() {
        if (SearchUrl.title != null) {
            title.setText(SearchUrl.title)
        }

        if (SearchUrl.fieldOffices != null) {
            fieldOffice.setText(SearchUrl.fieldOffices)
        }

        val arrayAdapterClassification = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, listOf("") + PersonClassification.values().map { it.toString() })
        classification.setAdapter(arrayAdapterClassification)
        if (SearchUrl.personClassification != null) {
            classification.setText(SearchUrl.personClassification.toString(), false)
        }

        val arrayAdapterStatus = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, listOf("") + PersonStatus.values().map { it.toString() })
        status.setAdapter(arrayAdapterStatus)
        if (SearchUrl.status != null) {
            status.setText(SearchUrl.status.toString(), false)
        }

        val arrayAdapterPageSize = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, listOf(10, 25, 50, 100))
        pageSize.setAdapter(arrayAdapterPageSize)
        pageSize.setText(SearchUrl.pageSize.toString(), false)

        val arrayAdapterSortOn = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, SortOn.values().map { it.toString() })
        sortOn.setAdapter(arrayAdapterSortOn)
        sortOn.setText(SearchUrl.sortOn.toString(), false)

        val arrayAdapterSortOrder = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, SortOrder.values().map { it.toString() })
        sortOrder.setAdapter(arrayAdapterSortOrder)
        sortOrder.setText(SearchUrl.sortOrder.toString(), false)
    }

    fun search(view: View) {
        val titleValue = title.text?.toString()
        SearchUrl.title = if (titleValue.isNullOrEmpty()) null else titleValue

        val fieldOfficeValue = fieldOffice.text?.toString()
        SearchUrl.fieldOffices = if (fieldOfficeValue.isNullOrEmpty()) null else fieldOfficeValue

        val classificationValue = classification.editableText?.toString()
        SearchUrl.personClassification = if (classificationValue.isNullOrEmpty()) null else PersonClassification.valueOf(classificationValue)

        val statusValue = status.editableText?.toString()
        SearchUrl.status = if (statusValue.isNullOrEmpty()) null else PersonStatus.valueOf(statusValue)

        val pageSizeValue = pageSize.editableText.toString().toInt()
        SearchUrl.pageSize = pageSizeValue

        val sortOnValue = sortOn.editableText.toString()
        SearchUrl.sortOn = SortOn.valueOf(sortOnValue)

        val sortOrderValue = sortOrder.editableText.toString()
        SearchUrl.sortOrder = SortOrder.valueOf(sortOrderValue)

        SearchUrl.page = 1

        val returnIntent = Intent()
        returnIntent.putExtra("backPressed", false)
        setResult(Activity.RESULT_OK, returnIntent)
        finish()
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return false
    }

    override fun onBackPressed() {
        val returnIntent = Intent()
        returnIntent.putExtra("backPressed", true)
        setResult(Activity.RESULT_OK, returnIntent)
        super.onBackPressed()
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }
}
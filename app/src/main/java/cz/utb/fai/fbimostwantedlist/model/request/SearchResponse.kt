package cz.utb.fai.fbimostwantedlist.model.request

import cz.utb.fai.fbimostwantedlist.model.person.Person

data class SearchResponse(
        val total: Int,
        val items: List<Person>,
        val page: Int
)
package cz.utb.fai.fbimostwantedlist.util

object RequestCodes {
    const val SEARCH_ACTIVITY_REQUEST_CODE = 1
    const val SEARCH_RESULTS_ACTIVITY_REQUEST_CODE = 2
    const val STORED_PERSONS_ACTIVITY_REQUEST_CODE = 3
    const val DETAIL_ACTIVITY_REQUEST_CODE = 4
}
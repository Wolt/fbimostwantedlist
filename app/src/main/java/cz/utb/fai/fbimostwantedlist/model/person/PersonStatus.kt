package cz.utb.fai.fbimostwantedlist.model.person

enum class PersonStatus {
    na,
    captured,
    recovered,
    located,
    surrendered,
    deceased
}
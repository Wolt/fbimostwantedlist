package cz.utb.fai.fbimostwantedlist.activity

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.animation.AlphaAnimation
import android.widget.FrameLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.toolbox.Volley
import com.google.gson.reflect.TypeToken
import cz.utb.fai.fbimostwantedlist.R
import cz.utb.fai.fbimostwantedlist.adapter.MainRecyclerViewAdapter
import cz.utb.fai.fbimostwantedlist.model.request.GsonGetRequest
import cz.utb.fai.fbimostwantedlist.model.request.SearchResponse
import cz.utb.fai.fbimostwantedlist.model.search.SearchResultHistory
import cz.utb.fai.fbimostwantedlist.model.search.SearchUrl
import cz.utb.fai.fbimostwantedlist.util.FileConstants
import cz.utb.fai.fbimostwantedlist.util.RequestCodes
import cz.utb.fai.fbimostwantedlist.util.Utils


class MainActivity : AppCompatActivity() {
    private val TAG = this::class.java.name

    private lateinit var recyclerView: RecyclerView
    private lateinit var mainRecyclerViewAdapter: MainRecyclerViewAdapter
    private lateinit var progressBarHolder: FrameLayout

    private lateinit var inAnimation: AlphaAnimation
    private lateinit var outAnimation: AlphaAnimation

    override fun onCreate(savedInstanceState: Bundle?) {
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView = findViewById(R.id.recycler_view_main)

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1) && newState == RecyclerView.SCROLL_STATE_IDLE) {
                    toggleProgressBar()
                    SearchUrl.page++
                    getAdditionalData()
                }
            }
        })

        recyclerView.layoutManager = LinearLayoutManager(this)
        progressBarHolder = findViewById(R.id.progressBarHolderMain)

        toggleProgressBar()
        getInitialData()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RequestCodes.SEARCH_ACTIVITY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                val hasBackBeenPressed = data?.getBooleanExtra("backPressed", false) ?: false
                if (!hasBackBeenPressed) {
                    toggleProgressBar()
                    getInitialData()
                }
            }
        }

        if (requestCode == RequestCodes.SEARCH_RESULTS_ACTIVITY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                val hasBackBeenPressed = data?.getBooleanExtra("backPressed", false) ?: false
                if (!hasBackBeenPressed) {
                    toggleProgressBar()
                    getInitialData()
                }
            }
        }
    }

    private fun getInitialData() {
        val queue = Volley.newRequestQueue(this)
        val url = SearchUrl.getUrl()
        val gsonRequest = GsonGetRequest(
                url,
                SearchResponse::class.java,
                null,
                {
                    mainRecyclerViewAdapter = MainRecyclerViewAdapter(this, it.items) { person ->
                        Log.i(TAG, person.toString())
                        val intent = Intent(this, DetailActivity::class.java)
                        intent.putExtra("person", person)
                        startActivityForResult(intent, RequestCodes.DETAIL_ACTIVITY_REQUEST_CODE)
                    }
                    recyclerView.adapter = mainRecyclerViewAdapter

                    if (it.total > 0) {
                        saveToLocalStorage(it.total)
                    }

                    toggleProgressBar()
                },
                {
                    Toast.makeText(
                            applicationContext,
                            "Ooopsie ... something did not work ...",
                            Toast.LENGTH_LONG
                    ).show()
                    Log.d(TAG, it.toString())
                    toggleProgressBar()
                })

        queue.add(gsonRequest)
    }

    private fun getAdditionalData() {
        val queue = Volley.newRequestQueue(this)
        val url = SearchUrl.getUrl()
        val gsonRequest = GsonGetRequest(
                url,
                SearchResponse::class.java,
                null,
                {
                    if (it.items.isEmpty()) {
                        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {})
                    } else {
                        mainRecyclerViewAdapter.persons = mainRecyclerViewAdapter.persons + it.items
                        mainRecyclerViewAdapter.notifyDataSetChanged()

                    }
                    toggleProgressBar()
                },
                {
                    Toast.makeText(
                            applicationContext,
                            "Ooopsie ... something did not work ...",
                            Toast.LENGTH_LONG
                    ).show()
                    Log.d(TAG, it.toString())
                    toggleProgressBar()
                })

        queue.add(gsonRequest)
    }

    fun search(view: View) {
        startActivityForResult(Intent(this, SearchActivity::class.java), RequestCodes.SEARCH_ACTIVITY_REQUEST_CODE)
    }

    private fun toggleProgressBar() {
        when (progressBarHolder.visibility) {
            View.VISIBLE -> {
                outAnimation = AlphaAnimation(1f, 0f)
                outAnimation.duration = 200
                progressBarHolder.animation = outAnimation
                progressBarHolder.visibility = View.GONE
            }
            View.GONE -> {
                inAnimation = AlphaAnimation(0f, 1f)
                inAnimation.duration = 200
                progressBarHolder.animation = inAnimation
                progressBarHolder.visibility = View.VISIBLE
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.options, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == R.id.searchResults) {
            startActivityForResult(Intent(this, SearchResultsActivity::class.java), RequestCodes.SEARCH_RESULTS_ACTIVITY_REQUEST_CODE)
            return true
        }

        if (id == R.id.storedPersons) {
            startActivityForResult(Intent(this, StoredPersonsActivity::class.java), RequestCodes.STORED_PERSONS_ACTIVITY_REQUEST_CODE)
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun saveToLocalStorage(total: Int) {
        val gson = Utils.genericGson
        val sharedPreferences: SharedPreferences = this.getSharedPreferences(FileConstants.SEARCH_RESULTS_PREFERENCE_FILE, MODE_PRIVATE)

        val setOfSearchHistoryJson = sharedPreferences.getString(FileConstants.SEARCH_RESULTS_KEY, "[]")!!
        val setOfSearchHistory: MutableSet<SearchResultHistory> = gson.fromJson(setOfSearchHistoryJson, object : TypeToken<MutableSet<SearchResultHistory>>() {}.type)

        val searchResultHistory = SearchResultHistory(
                SearchUrl.pageSize,
                SearchUrl.sortOn,
                SearchUrl.sortOrder,
                SearchUrl.title,
                SearchUrl.fieldOffices,
                SearchUrl.personClassification,
                SearchUrl.status,
                total
        )

        setOfSearchHistory.add(searchResultHistory)

        with(sharedPreferences.edit()) {
            putString(FileConstants.SEARCH_RESULTS_KEY, gson.toJson(setOfSearchHistory))
            apply()
        }
    }
}


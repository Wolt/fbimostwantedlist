package cz.utb.fai.fbimostwantedlist.model.search

enum class SortOrder(val value: String) {
    DESC("desc"),
    ASC("asc")
}
package cz.utb.fai.fbimostwantedlist.model.person

enum class PersonClassification {
    main,
    victim,
    accomplice
}
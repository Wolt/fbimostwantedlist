package cz.utb.fai.fbimostwantedlist.model.person

import android.os.Parcel
import android.os.Parcelable

data class FBIImage(val thumb: String?, val large: String?, val original: String?, val caption: String?) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(thumb)
        parcel.writeString(large)
        parcel.writeString(original)
        parcel.writeString(caption)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<FBIImage> {
        override fun createFromParcel(parcel: Parcel): FBIImage {
            return FBIImage(parcel)
        }

        override fun newArray(size: Int): Array<FBIImage?> {
            return arrayOfNulls(size)
        }
    }
}
package cz.utb.fai.fbimostwantedlist.adapter

import android.content.Context
import android.content.SharedPreferences
import android.text.Html
import android.text.TextUtils
import android.view.ContextMenu
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.PopupMenu
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.reflect.TypeToken
import com.squareup.picasso.Picasso
import cz.utb.fai.fbimostwantedlist.R
import cz.utb.fai.fbimostwantedlist.model.person.Person
import cz.utb.fai.fbimostwantedlist.util.FileConstants
import cz.utb.fai.fbimostwantedlist.util.Utils


class StoredPersonsAdapter(val context: Context, var persons: Set<Person>, private val onItemClickListener: (Person) -> Unit) : RecyclerView.Adapter<StoredPersonsAdapter.StoredPersonViewHolder>() {
    class StoredPersonViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnCreateContextMenuListener {
        val imageView: ImageView
        val textView: TextView
        val cardView: CardView

        init {
            imageView = view.findViewById(R.id.thumbnail) as ImageView
            textView = view.findViewById(R.id.title) as TextView
            cardView = view.findViewById(R.id.cardview) as CardView
        }

        override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {
            menu!!.add(0, 1, 0, "Delete")
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): StoredPersonViewHolder {
        val view: View = LayoutInflater.from(viewGroup.context).inflate(R.layout.list_row, null)
        return StoredPersonViewHolder(view)
    }

    override fun onBindViewHolder(storedPersonViewHolder: StoredPersonViewHolder, i: Int) {
        val person = persons.elementAt(i)

        if (!TextUtils.isEmpty(person.images[0].thumb)) {
            Picasso.with(context).load(person.images[0].thumb)
                    .error(R.drawable.placeholder)
                    .placeholder(R.drawable.placeholder)
                    .into(storedPersonViewHolder.imageView)
        }

        storedPersonViewHolder.textView.text = Html.fromHtml(person.title, Html.FROM_HTML_MODE_LEGACY)

        val listener: View.OnClickListener = View.OnClickListener {
            onItemClickListener.invoke(person)
        }
        storedPersonViewHolder.cardView.setOnClickListener(listener)

        storedPersonViewHolder.cardView.setOnLongClickListener {
            val pop = PopupMenu(context, it)
            pop.inflate(R.menu.context_menu)
            pop.setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.menu_custom_delete -> {
                        val p = persons.toMutableSet()
                        p.remove(person)
                        persons = p.toSet()
                        removeFromLocalStorage(person)
                        this.notifyDataSetChanged()
                    }
                }
                true
            }
            pop.show()
            true
        }
    }

    override fun getItemCount() = persons.size

    private fun removeFromLocalStorage(person: Person) {
        val gson = Utils.genericGson
        val sharedPreferences: SharedPreferences = context.getSharedPreferences(FileConstants.STORED_PERSONS_PREFERENCE_FILE, AppCompatActivity.MODE_PRIVATE)

        val setOfSavedPersonsJson = sharedPreferences.getString(FileConstants.STORED_PERSONS_KEY, "[]")!!
        val setOfSavedPersons: MutableMap<String, Person> = gson.fromJson(setOfSavedPersonsJson, object : TypeToken<MutableMap<String, Person>>() {}.type)

        setOfSavedPersons.remove(person.uid)

        with(sharedPreferences.edit()) {
            putString(FileConstants.STORED_PERSONS_KEY, gson.toJson(setOfSavedPersons))
            apply()
        }
    }
}
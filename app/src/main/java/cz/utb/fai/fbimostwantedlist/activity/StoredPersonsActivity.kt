package cz.utb.fai.fbimostwantedlist.activity

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.reflect.TypeToken
import cz.utb.fai.fbimostwantedlist.R
import cz.utb.fai.fbimostwantedlist.adapter.StoredPersonsAdapter
import cz.utb.fai.fbimostwantedlist.model.person.Person
import cz.utb.fai.fbimostwantedlist.util.FileConstants
import cz.utb.fai.fbimostwantedlist.util.Utils


class StoredPersonsActivity : AppCompatActivity() {
    val TAG = this::class.java.name

    lateinit var storedPersonsAdapter: StoredPersonsAdapter
    lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stored_persons)

        supportActionBar?.title = "Stored Persons"

        recyclerView = findViewById(R.id.recycler_view_stored_persons)
        recyclerView.layoutManager = LinearLayoutManager(this)

        val gson = Utils.genericGson
        val sharedPreferences: SharedPreferences = this.getSharedPreferences(FileConstants.STORED_PERSONS_PREFERENCE_FILE, MODE_PRIVATE)

        val setOfSavedPersonsJson = sharedPreferences.getString(FileConstants.STORED_PERSONS_KEY, "[]")!!
        val setOfSavedPersons: MutableMap<String, Person> = gson.fromJson(setOfSavedPersonsJson, object : TypeToken<MutableMap<String, Person>>() {}.type)

        storedPersonsAdapter = StoredPersonsAdapter(this, setOfSavedPersons.map { it.value }.toSet()) { person ->
            Log.i(TAG, person.toString())
            val intent = Intent(this, DetailActivity::class.java)
            intent.putExtra("person", person)
            startActivity(intent)
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
        }
        recyclerView.adapter = storedPersonsAdapter
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return false
    }

    override fun onBackPressed() {
        val returnIntent = Intent()
        returnIntent.putExtra("backPressed", true)
        setResult(Activity.RESULT_OK, returnIntent)
        super.onBackPressed()
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }
}
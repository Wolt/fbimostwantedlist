package cz.utb.fai.fbimostwantedlist.adapter

import android.content.Context
import android.text.Html
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import cz.utb.fai.fbimostwantedlist.R
import cz.utb.fai.fbimostwantedlist.model.person.Person


class MainRecyclerViewAdapter(private val context: Context, var persons: List<Person>, private val onItemClickListener: (Person) -> Unit) : RecyclerView.Adapter<MainRecyclerViewAdapter.MainViewHolder>() {
    class MainViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val imageView: ImageView
        val textView: TextView
        val cardView: CardView

        init {
            imageView = view.findViewById(R.id.thumbnail) as ImageView
            textView = view.findViewById(R.id.title) as TextView
            cardView = view.findViewById(R.id.cardview) as CardView
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MainViewHolder {
        val view: View = LayoutInflater.from(viewGroup.context).inflate(R.layout.list_row, null)
        return MainViewHolder(view)
    }

    override fun onBindViewHolder(mainViewHolder: MainViewHolder, i: Int) {
        val person = persons[i]

        //Render image using Picasso library
        if (!TextUtils.isEmpty(person.images[0].thumb)) {
            Picasso.with(context).load(person.images[0].thumb)
                    .error(R.drawable.placeholder)
                    .placeholder(R.drawable.placeholder)
                    .into(mainViewHolder.imageView)
        }

        //Setting text view title
        mainViewHolder.textView.text = Html.fromHtml(person.title)

        val listener: View.OnClickListener = View.OnClickListener {
            onItemClickListener.invoke(person)
        }
        mainViewHolder.imageView.setOnClickListener(listener)
        mainViewHolder.textView.setOnClickListener(listener)
        mainViewHolder.cardView.setOnClickListener(listener)
    }

    override fun getItemCount() = persons.size
}
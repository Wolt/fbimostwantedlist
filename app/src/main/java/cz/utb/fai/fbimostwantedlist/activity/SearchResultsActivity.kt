package cz.utb.fai.fbimostwantedlist.activity

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.reflect.TypeToken
import cz.utb.fai.fbimostwantedlist.R
import cz.utb.fai.fbimostwantedlist.adapter.SearchResultsRecyclerViewAdapter
import cz.utb.fai.fbimostwantedlist.model.search.SearchResultHistory
import cz.utb.fai.fbimostwantedlist.model.search.SearchUrl
import cz.utb.fai.fbimostwantedlist.util.FileConstants
import cz.utb.fai.fbimostwantedlist.util.Utils

class SearchResultsActivity : AppCompatActivity() {
    val TAG = this::class.java.name

    override fun onCreate(savedInstanceState: Bundle?) {
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_results)

        supportActionBar?.title = "Search Results"

        val recyclerView: RecyclerView = findViewById(R.id.recycler_view_search_results)
        recyclerView.layoutManager = LinearLayoutManager(this)

        val gson = Utils.genericGson
        val sharedPreferences: SharedPreferences = this.getSharedPreferences(FileConstants.SEARCH_RESULTS_PREFERENCE_FILE, MODE_PRIVATE)

        val setOfSearchHistoryJson = sharedPreferences.getString(FileConstants.SEARCH_RESULTS_KEY, "[]")!!
        val setOfSearchHistory: Set<SearchResultHistory> = gson.fromJson(setOfSearchHistoryJson, object : TypeToken<MutableSet<SearchResultHistory>>() {}.type)

        val adapter = SearchResultsRecyclerViewAdapter(this, setOfSearchHistory) { searchHistory ->
            SearchUrl.title = searchHistory.title
            SearchUrl.fieldOffices = searchHistory.fieldOffices
            SearchUrl.page = 1
            SearchUrl.sortOrder = searchHistory.sortOrder
            SearchUrl.sortOn = searchHistory.sortOn
            SearchUrl.pageSize = searchHistory.pageSize
            SearchUrl.status = searchHistory.status
            SearchUrl.personClassification = searchHistory.personClassification

            val returnIntent = Intent()
            returnIntent.putExtra("backPressed", false)
            setResult(Activity.RESULT_OK, returnIntent)
            finish()
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
        }

        recyclerView.adapter = adapter
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return false
    }

    override fun onBackPressed() {
        val returnIntent = Intent()
        returnIntent.putExtra("backPressed", true)
        setResult(Activity.RESULT_OK, returnIntent)
        super.onBackPressed()
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }
}
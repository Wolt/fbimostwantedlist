package cz.utb.fai.fbimostwantedlist.util

import com.google.gson.*
import java.sql.Date
import java.text.SimpleDateFormat
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter

object Utils {

    val genericGson: Gson = GsonBuilder().registerTypeAdapter(LocalDateTime::class.java, JsonDeserializer { json, _, _ ->
        json.asJsonPrimitive.asString.toLocalDateTime()
    }).registerTypeAdapter(LocalDateTime::class.java, JsonSerializer<LocalDateTime> { src, _, _ ->
        JsonPrimitive(src.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME))
    }).create()

    val requestGson: Gson = GsonBuilder().registerTypeAdapter(
            LocalDateTime::class.java,
            JsonDeserializer { json, _, _ ->
                json.asJsonPrimitive.asString.toLocalDateTime()
            })
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .create()

    fun <T> transformIfNotNull(str: String?, lambda: (String) -> T): T? {
        return if (str == null) {
            null
        } else {
            lambda.invoke(str)
        }
    }

    fun String.toLocalDateTime(): LocalDateTime {
        val pattern = "yyyy-MM-dd'T'HH:mm:ss"
        val sdf = SimpleDateFormat(pattern)
        val date = sdf.parse(this) ?: Date.from(Instant.now())
        return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault())
    }
}